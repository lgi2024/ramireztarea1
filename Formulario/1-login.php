<html>
<head>
    <title>Log</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .form-container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <form action="1-validar_login.php" method="post">
            <p>Ingresa tu usuario: <br />
            <input type="text" size="50" name="nombre" required /></p>
            <p>Ingresa tu contrasenia: <br />
            <input type="password" size="50" name="contra1" required></p>
            <input type="submit" value="Iniciar sesión" />
        </form>
        <br>
        <button onclick="window.location.href='1-registro.php';">Registrarse</button>
    </div>
</body>
</html>
