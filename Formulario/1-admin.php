<?php
session_start();
if (!isset($_SESSION['nombre']) || $_SESSION['nombre'] != 'mateo') {
    header("Location: 1-login.php");
    exit();
}

include 'conexionform.php';

// Manejar la creación de un nuevo producto
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['crear'])) {
    $nombreproducto = $_POST['nombreproducto'];
    $precio = $_POST['precio'];

    $sql = "INSERT INTO productos (nombreproducto, precio) VALUES ('$nombreproducto', '$precio')";
    if (mysqli_query($conn, $sql)) {
        echo "Nuevo producto creado exitosamente.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Manejar la eliminación de un producto
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['eliminar'])) {
    $idproducto = $_POST['idproducto'];

    $sql = "DELETE FROM productos WHERE idproducto = '$idproducto'";
    if (mysqli_query($conn, $sql)) {
        echo "Producto eliminado exitosamente.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Manejar la modificación de un producto
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['modificar'])) {
    $idproducto = $_POST['idproducto'];
    $nombreproducto = $_POST['nombreproducto'];
    $precio = $_POST['precio'];

    $sql = "UPDATE productos SET nombreproducto = '$nombreproducto', precio = '$precio' WHERE idproducto = '$idproducto'";
    if (mysqli_query($conn, $sql)) {
        echo "Producto modificado exitosamente.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}

// Obtener todos los productos para mostrarlos en la interfaz
$sql = "SELECT idproducto, nombreproducto, precio FROM productos";
$result = mysqli_query($conn, $sql);

?>

<html>
<head>
    <title>Administrar Productos</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .form-container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h2>Administrar Productos</h2>

        <h3>Crear Nuevo Producto</h3>
        <form action="1-admin.php" method="post">
            <p>Nombre del producto: <br />
            <input type="text" size="50" name="nombreproducto" required /></p>
            <p>Precio: <br />
            <input type="text" size="50" name="precio" required></p>
            <input type="submit" name="crear" value="Crear Producto" />
        </form>

        <h3>Eliminar Producto</h3>
        <form action="1-admin.php" method="post">
            <p>ID del producto a eliminar: <br />
            <input type="text" size="50" name="idproducto" required /></p>
            <input type="submit" name="eliminar" value="Eliminar Producto" />
        </form>

        <h3>Modificar Producto</h3>
        <form action="1-admin.php" method="post">
            <p>ID del producto a modificar: <br />
            <input type="text" size="50" name="idproducto" required /></p>
            <p>Nuevo nombre del producto: <br />
            <input type="text" size="50" name="nombreproducto" required /></p>
            <p>Nuevo precio: <br />
            <input type="text" size="50" name="precio" required></p>
            <input type="submit" name="modificar" value="Modificar Producto" />
        </form>

        <h3>Lista de Productos</h3>
        <?php
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                echo "ID: " . $row["idproducto"]. " - Producto: " . $row["nombreproducto"]. " - Precio: $" . $row["precio"] . "<br>";
            }
        } else {
            echo "No hay productos disponibles.";
        }
        mysqli_close($conn);
        ?>
    </div>
</body>
</html>