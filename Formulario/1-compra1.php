<?php
session_start();
if (!isset($_SESSION['nombre'])) {
    header("Location: 1-login.php");
    exit();
}
?>

<html>
<head> 
    <title>Compra</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .form-container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <form action="1-compra2.php" method="post">
            <p>Usuario: <?php echo $_SESSION['nombre']; ?></p>
            <p>Tipo entrega: 
                <select name="select">
                    <option value="1">Entrega</option>
                    <option value="2">Delivery</option>
                </select>
            </p>
            <p><input type="submit" value="Realizar pedido" /></p>
        
        <?php
        include 'conexionform.php';

        $sql = "SELECT idproducto, nombreproducto, precio FROM productos";
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row 
            while($row = mysqli_fetch_assoc($result)) {
                // Crear un checkbox para cada producto
                echo '<input type="checkbox" name="productos[]" value="' . $row["idproducto"] . '"> ';
                echo "Producto: " . $row["nombreproducto"]. " - Precio: $" . $row["precio"] . "<br>";
            }
        } else {
            echo "No hay productos disponibles.";
        }
        mysqli_close($conn);
        ?>
        </form>
    </div>
</body>
</html>