<html>
<head>
    <title>Compra</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .form-container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <font size='+1'>
        <?php
        $precio_total = 0;

        include 'conexionform.php';
        $productos = isset($_POST["productos"]) ? $_POST["productos"] : array();
        $tipo_entrega = isset($_POST["select"]) ? $_POST["select"] : null;

        if (empty($productos)) {
            echo "No se seleccionó ninguna opción";
        } else {
            for ($i = 0; $i < count($productos); $i++) {
                $idproducto = $productos[$i];
                $sql = "SELECT nombreproducto, precio FROM productos WHERE idproducto = " . $idproducto;
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) > 0) {
                    // output data of each row
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo "Nombre: " . $row["nombreproducto"] . " - Precio: $" . $row["precio"] . " " . "<br>";
                        $precio_total = $precio_total + $row["precio"];
                    }
                } else {
                    echo "0 results";
                }
            }
            if ($tipo_entrega == 1) {
                echo "Precio total: $precio_total";
            }
            if ($tipo_entrega == 2) {
                $precio_total += 10;
                echo "Precio total: $precio_total (Precio total + delivery)";
            }
        }

        mysqli_close($conn);
        ?>
        </font>

        <p>
            <input type="button" onclick="location.href='1-compra1.php';" value="Volver" /><p>
    </div>
</body>
</html>