<html>
<head>
    <title>Registro</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .form-container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.1);
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-container">
        <h2>Registro de Usuario</h2>
        <form action="1-registro.php" method="post">
            <p>Nombre de usuario: <br />
            <input type="text" size="50" name="nombre" required /></p>
            <p>Contraseña: <br />
            <input type="password" size="50" name="contra1" required></p>
            <p>Edad: <br />
            <input type="text" size="50" name="edad" required /></p>
            <p>Correo electrónico: <br />
            <input type="text" size="50" name="email" required /></p>
            <input type="submit" value="Registrarse" />
        </form>

        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $nombre = $_POST['nombre'];
            $contrasenia = $_POST['contra1'];
            $edad = $_POST['edad'];
            $email = $_POST['email'];

            // Validación de edad
            if (!is_numeric($edad) || $edad <= 0) {
                echo "La edad debe ser un número positivo.<br>";
            }
            // Validación de correo electrónico
            elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo "Por favor, ingresa un correo electrónico válido.<br>";
            } else {
                include 'conexionform.php';

                $sql = "INSERT INTO usuarios (usuario, clave, edad, email) VALUES ('$nombre', '$contrasenia', '$edad', '$email')";
                if (mysqli_query($conn, $sql)) {
                    echo "Registro exitoso. Puedes <a href='1-login.php'>iniciar sesión</a>.";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }

                mysqli_close($conn);
            }
        }
        ?>
    </div>
</body>
</html>
