<?php
session_start();
include 'conexioncrud.php';

// Verificar si el formulario fue enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $username = mysqli_real_escape_string($conn, $username);
    $password = mysqli_real_escape_string($conn, $password);

    // Verificar usuario y contraseña con consulta preparada
    $stmt = $conn->prepare("SELECT id, username, role FROM users WHERE username=? AND password=?");
    $stmt->bind_param("ss", $username, $password);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        // Guardar datos del usuario en la sesión
        $_SESSION['userid'] = $row['id'];
        $_SESSION['username'] = $row['username'];
        $_SESSION['role'] = $row['role'];

        // Redirigir al dashboard
        header("Location: dashboard.php");
        exit();
    } else {
        echo "Nombre de usuario o contraseña incorrectos.";
    }

    $stmt->close();
    mysqli_close($conn); // Cerrar la conexión
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Iniciar Sesión</title>
    <style>
        /* Estilos para el cuerpo */
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: white;
        }
        .login-container {
            background-color: gray;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }
        input[type="text"], input[type="password"] {
            margin-bottom: 10px;
            padding: 10px;
            width: 100%;
            border: none;
            border-radius: 5px;
        }
        input[type="submit"] {
            background-color: #333;
            color: white;
            padding: 10px;
            width: 100%;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #555;
        }
    </style>
    <script>
        function validateForm(event) {
            const username = document.querySelector('input[name="username"]').value;
            const password = document.querySelector('input[name="password"]').value;

            if (!username || !password) {
                alert("Por favor, complete todos los campos.");
                event.preventDefault(); // Prevenir el envío del formulario
            }
        }
    </script>
</head>
<body>
    <div class="login-container">
        <h1>Iniciar Sesión</h1>
        <!-- Formulario de inicio de sesión -->
        <form method="post" action="" onsubmit="return validarFormulario()">
            Nombre de usuario: <input type="text" id="username" name="username" required><br>
            Contraseña: <input type="password" id="password" name="password" required><br>
            <input type="submit" value="Iniciar Sesión">
        </form>
    </div>

    <script>
        function validarFormulario() {
            let username = document.getElementById("username").value;
            let password = document.getElementById("password").value;

            if (username === "") {
                alert("Por favor, ingresa tu nombre de usuario.");
                return false;
            }

            if (password === "") {
                alert("Por favor, ingresa tu contraseña.");
                return false;
            }

            return true;
        }
    </script>
</body>
</html>