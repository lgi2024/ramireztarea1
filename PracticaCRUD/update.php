<?php
session_start();

// Verificar si el usuario está logueado
if (!isset($_SESSION['userid'])) {
    header("Location: login.php");
    exit();
}

// Verificar si el usuario es administrador
if ($_SESSION['role'] !== 'admin') {
    echo "No tienes permiso para realizar esta acción.";
    exit();
}

include 'conexioncrud.php'; // Incluir conexión a la base de datos

$id = $_GET['id'];

// Verificar si el formulario fue enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $profilePic = $_FILES['profile_pic']['name'];

    // Actualizar la imagen de perfil si se proporciona
    if ($profilePic) {
        $targetDir = __DIR__ . "/images/";
        $targetFile = $targetDir . basename($profilePic);
        move_uploaded_file($_FILES['profile_pic']['tmp_name'], $targetFile);
    } else {
        // Mantener la imagen existente si no se proporciona una nueva
        $profilePic = $_POST['current_pic'];
    }

    // Actualizar el estudiante en la base de datos
    $sql = "UPDATE students SET name='$name', age='$age', email='$email', profile_pic='$profilePic' WHERE id='$id'";
    if (mysqli_query($conn, $sql)) {
        echo "Estudiante actualizado exitosamente.";
    } else {
        echo "Error actualizando estudiante: " . mysqli_error($conn);
    }

    mysqli_close($conn); // Cerrar la conexión
    exit();
}

// Obtener datos del estudiante para mostrar en el formulario
$sql = "SELECT id, name, age, email, profile_pic FROM students WHERE id='$id'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $student = mysqli_fetch_assoc($result);
} else {
    echo "Estudiante no encontrado.";
    mysqli_close($conn); // Cerrar la conexión
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Estudiante</title>
    <style>
        /* Estilos para el cuerpo */
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: white;
        }
        .container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px white;
        }
        input[type="text"], input[type="email"], input[type="number"] {
            margin-bottom: 10px;
            padding: 10px;
            width: 100%;
            border: none;
            border-radius: 5px;
        }
        input[type="submit"] {
            background-color: #333;
            color: white;
            padding: 10px;
            width: 100%;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
        input[type="submit"]:hover {
            background-color: #555;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Editar Estudiante</h1>
        <!-- Formulario para editar estudiante -->
        <form method="post" action="" enctype="multipart/form-data">
            Nombre: <input type="text" name="name" value="<?php echo $student['name']; ?>" required><br>
            Edad: <input type="number" name="age" value="<?php echo $student['age']; ?>" required><br>
            Correo Electrónico: <input type="email" name="email" value="<?php echo $student['email']; ?>" required><br>
            Foto de Perfil: <input type="file" name="profile_pic"><br>
            <input type="hidden" name="current_pic" value="<?php echo $student['profile_pic']; ?>">
            <input type="submit" value="Guardar Cambios">
        </form>
    </div>
</body>
</html>