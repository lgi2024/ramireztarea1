<?php
session_start();
include 'conexioncrud.php';

// Obtener el campo por el cual se ordenará y la dirección de la ordenación (ASC o DESC)
$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : 'id';
$order_dir = isset($_GET['order_dir']) ? $_GET['order_dir'] : 'ASC';

// Alternar entre ASC y DESC para la siguiente ordenación
$order_dir = $order_dir === 'ASC' ? 'DESC' : 'ASC';

// Obtener la búsqueda
$search = isset($_GET['search']) ? $_GET['search'] : '';

// Construir la consulta SQL con la ordenación y la búsqueda
$sql = "SELECT id, name, age, email, profile_pic FROM students WHERE name LIKE '%$search%' ORDER BY $order_by $order_dir";
$result = mysqli_query($conn, $sql);

// Obtener el rol del usuario desde la sesión
$role = isset($_SESSION['role']) ? $_SESSION['role'] : '';
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Estudiantes</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: white;
        }
        .container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px white;
            max-width: 80%;
            overflow: auto;
        }
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid white;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #333;
            cursor: pointer;
        }
        td img {
            max-width: 100px;
            max-height: 100px;
            object-fit: cover;
        }
        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
        }
    </style>
    <script>
        function fetchData() {
            const search = document.getElementById('search').value;
            const orderBy = document.getElementById('order_by').value;
            const orderDir = document.getElementById('order_dir').value;

            const xhr = new XMLHttpRequest();
            xhr.open('GET', `read.php?search=${search}&order_by=${orderBy}&order_dir=${orderDir}`, true);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    const response = xhr.responseText;
                    const parser = new DOMParser();
                    const doc = parser.parseFromString(response, 'text/html');
                    document.getElementById('data-table').innerHTML = doc.getElementById('data-table').innerHTML;
                }
            };
            xhr.send();
        }

        function sortTable(orderBy) {
            const currentOrderDir = document.getElementById('order_dir').value;
            const newOrderDir = currentOrderDir === 'ASC' ? 'DESC' : 'ASC';
            document.getElementById('order_by').value = orderBy;
            document.getElementById('order_dir').value = newOrderDir;
            fetchData();
        }

        function details() {
            document.addEventListener("click", e=>{
                if (e.target.matches(".detalles")){
                    let row = e.target.closest("tr");
                    if (row) {
                        let id = row.querySelectorAll("td")[0].textContent;
                        let name = row.querySelectorAll("td")[1].textContent;
                        let age = row.querySelectorAll("td")[2].textContent;
                        let email = row.querySelectorAll("td")[3].textContent;
                        alert(`Id: ${id} \nNombre: ${name} \nEdad: ${age} \nEmail: ${email}`);
                    }
                }
            })
        }
        details();

    </script>
</head>
<body>

<div class="container">
    <h1>Lista de Estudiantes</h1>

    <div class="header">
        <div class="search-box">
            <input type="text" id="search" onkeyup="fetchData()" placeholder="Buscar alumno">
        </div>

        <div class="buttons">
            <?php if ($role === 'admin') { ?>
                <form action="create.php" method="GET">
                    <button type="submit">Agregar/Añadir Estudiante</button>
                </form>
            <?php } ?>
            
            <form action="logout.php" method="POST">
                <button type="submit">Salir</button>
            </form>
        </div>
    </div>

    <input type="hidden" id="order_by" value="<?php echo $order_by; ?>">
    <input type="hidden" id="order_dir" value="<?php echo $order_dir; ?>">

    <div id="data-table">
        <?php
        if (mysqli_num_rows($result) > 0) {
            echo "<table>";
            echo "<tr>";
            echo "<th onclick=\"sortTable('id')\">ID</th>";
            echo "<th onclick=\"sortTable('name')\">Nombre</th>";
            echo "<th onclick=\"sortTable('age')\">Edad</th>";
            echo "<th onclick=\"sortTable('email')\">Correo Electrónico</th>";
            echo "<th>Foto de Perfil</th>";
            if ($role === 'admin') {
                echo "<th>Acciones</th>";
            }
            echo "</tr>";
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr>";
                echo "<td>" . $row["id"] . "</td>";
                echo "<td>" . $row["name"] . "</td>";
                echo "<td>" . $row["age"] . "</td>";
                echo "<td>" . $row["email"] . "</td>";
                echo "<td><img src='images/" . $row["profile_pic"] . "' alt='Foto de perfil'></td>";
                
                if ($role === 'admin') {
                    echo "<td class='actions'>";
                    echo "<a href='update.php?id=" . $row['id'] . "'>Editar</a>";
                    echo "<a href='delete.php?id=" . $row['id'] . "'>Eliminar</a>";
                    echo '<a class="detalles">Ver</a>';
                    echo "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        } else {
            echo "No se encontraron resultados.";
        }
        ?>
    </div>
</div>

</body>
</html>