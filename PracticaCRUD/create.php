<?php
session_start();

// Verificar si el usuario está logueado
if (!isset($_SESSION['userid'])) {
    header("Location: login.php");
    exit();
}

// Verificar si el usuario es administrador
if ($_SESSION['role'] !== 'admin') {
    echo "No tienes permiso para realizar esta acción.";
    exit();
}

// Verificar el método de solicitud POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $age = $_POST['age'];
    $email = $_POST['email'];
    $profilePic = $_FILES['profile_pic']['name'];

    // Subir imagen de perfil si se proporciona
    if ($profilePic) {
        $targetDir = __DIR__ . "/images/"; // Guardar en la carpeta "images"
        $targetFile = $targetDir . basename($profilePic);
        move_uploaded_file($_FILES['profile_pic']['tmp_name'], $targetFile);
    }

    include 'conexioncrud.php'; // Incluir conexión a la base de datos

    // Insertar nuevo estudiante
    $sql = "INSERT INTO students (name, age, email, profile_pic) VALUES ('$name', '$age', '$email', '$profilePic')";
    if (mysqli_query($conn, $sql)) {
        echo "Registro exitoso.";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn); // Cerrar la conexión
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Estudiante</title>
    <style>
        /* Estilos para el cuerpo */
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: white;
        }
        .container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px white;
        }
        input {
            margin-bottom: 10px;
            padding: 5px;
            width: 100%;
        }
    </style>
    <script>
        function validar() {
            const name = document.querySelector('input[name="name"]').value.trim();
            const age = document.querySelector('input[name="age"]').value.trim();
            const email = document.querySelector('input[name="email"]').value.trim();

            if (name === "") {
                alert("Por favor ingresa un nombre.");
                return false;
            }

            if (age === "" || isNaN(age) || age <= 0) {
                alert("Por favor ingresa una edad válida.");
                return false;
            }

            if (email === "" || !validateEmail(email)) {
                alert("Por favor ingresa un correo electrónico válido.");
                return false;
            }

            return true;
        }

        function validateEmail(email) {
            const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            return re.test(email);
        }
    </script>
</head>
<body>
    <div class="container">
        <h1>Crear Estudiante</h1>
        <!-- Formulario para crear un estudiante -->
        <form method="post" action="" enctype="multipart/form-data" onsubmit="return validar()">
            Nombre: <input type="text" id="name" name="name" required><br>
            Edad: <input type="number" id="age" name="age" required><br>
            Correo electrónico: <input type="email" id="email" name="email" required><br>
            Foto de perfil: <input type="file" id="profile_pic" name="profile_pic" accept="image/*"><br>
            <input type="submit" value="Crear">
        </form>
    </div>
    
    <script>
        function validar() {
            // Obtener los valores de los campos
            let name = document.getElementById("name").value;
            let age = document.getElementById("age").value;
            let email = document.getElementById("email").value;
            let profilePic = document.getElementById("profile_pic").value;

            if (name === "") {
                alert("Por favor ingresa un nombre.");
                return false;
            }

            if (age === "" || isNaN(age) || age <= 0) {
                alert("Por favor ingresa una edad válida.");
                return false;
            }

            // Validar formato del correo electrónico
            let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
            if (!emailRegex.test(email)) {
                alert("Por favor ingresa un correo electrónico válido.");
                return false;
            }

            // Validación opcional para verificar que se seleccionó una imagen
            if (profilePic === "") {
                alert("Por favor selecciona una imagen de perfil.");
                return false;
            }

            return true;
        }
    </script>
</body>
</html>