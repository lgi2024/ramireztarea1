<?php

session_start();

if (!isset($_SESSION['userid'])) {
    header("Location: login.php");
    exit();
}

if ($_SESSION['role'] !== 'admin') {
    echo "No tienes permiso para realizar esta acción.";
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Proceso de eliminación
    if (isset($_POST['id']) && !empty($_POST['id'])) {
        $id = intval($_POST['id']); // Convertir a entero para mayor seguridad

        include 'conexioncrud.php';

        $sql = "DELETE FROM students WHERE id=$id";

        if (mysqli_query($conn, $sql)) {
            echo "Estudiante eliminado con éxito";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }

        mysqli_close($conn);
    } else {
        echo "ID no proporcionado en la solicitud POST.";
    }
} else {
    // Mostrar el formulario de confirmación de eliminación
    if (isset($_GET['id']) && !empty($_GET['id'])) {
        $id = intval($_GET['id']); // Convertir a entero para mayor seguridad
        echo "ID recibido: $id<br>"; // Mensaje de depuración

        echo "<form method='post' action=''>
            <input type='hidden' name='id' value='$id'>
            ¿Estás seguro de que quieres eliminar al estudiante con ID: $id?<br>
            <input type='submit' value='Eliminar'>
        </form>";
    } else {
        echo "ID no proporcionado en la URL.";
    }
}
?>