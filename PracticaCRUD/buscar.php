<?php
include 'conexioncrud.php'; // Incluir conexión a la base de datos

// Obtener y limpiar datos de búsqueda
$searchName = isset($_POST['name']) ? trim($_POST['name']) : '';
$searchEmail = isset($_POST['email']) ? trim($_POST['email']) : '';
$searchAge = isset($_POST['age']) ? trim($_POST['age']) : '';
$sortBy = isset($_POST['sort_by']) ? $_POST['sort_by'] : 'id';
$order = isset($_POST['order']) ? $_POST['order'] : 'ASC';

// Verificar columnas válidas para ordenar
$validColumns = ['name', 'age', 'email'];
if (!in_array($sortBy, $validColumns)) {
    $sortBy = 'id';
}
if ($order !== 'ASC' && $order !== 'DESC') {
    $order = 'ASC';
}

// Crear consulta SQL básica
$sql = "SELECT id, name, email, age FROM students WHERE 1=1";

// Añadir filtros de búsqueda si se proporcionan
if (!empty($searchName)) {
    $searchName = mysqli_real_escape_string($conn, $searchName);
    $sql .= " AND name LIKE '%$searchName%'";
}

if (!empty($searchEmail)) {
    $searchEmail = mysqli_real_escape_string($conn, $searchEmail);
    $sql .= " AND email LIKE '%$searchEmail%'";
}

if (!empty($searchAge)) {
    $searchAge = mysqli_real_escape_string($conn, $searchAge);
    $sql .= " AND age = '$searchAge'";
}

// Añadir ordenación a la consulta SQL
$sql .= " ORDER BY $sortBy $order";

$result = mysqli_query($conn, $sql); // Ejecutar la consulta
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultados de Búsqueda</title>
    <style>
        /* Estilos para el cuerpo */
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            color: white;
        }
        .container {
            background-color: grey;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px 0px white;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid white;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #333;
        }
        .mostrar {
            background-color: green;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Resultados de Búsqueda</h1>
        <?php
        // Mostrar resultados si los hay
        if (mysqli_num_rows($result) > 0) {
            echo "<table><tr><th>ID</th><th>Nombre</th><th>Correo Electrónico</th><th>Edad</th></tr>";
            while ($row = mysqli_fetch_assoc($result)) {
                echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["age"] . "</td></tr>";
            }
            echo "</table>";
        } else {
            echo "No se encontraron resultados.";
        }
        mysqli_close($conn); // Cerrar la conexión
        ?>
    </div>
    <script>
        function mostrarColor(color) {}
            class.container.style.backgroundcolor = 'green';
    </script>
</body>
</html>