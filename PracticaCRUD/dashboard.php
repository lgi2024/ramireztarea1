<?php
session_start();

// Verificar si el usuario está logueado
if (!isset($_SESSION['userid'])) {
    header("Location: login.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <style>
        body {
            background-color: black;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .dashboard-container {
            background-color: gray;
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
            text-align: center;
        }
        .dashboard-container a {
            display: block;
            margin: 10px 0;
            color: white;
            text-decoration: none;
        }
        .dashboard-container a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div class="dashboard-container">
        <?php
        echo "<h1>Bienvenido, " . $_SESSION['username'] . "</h1>";

        if ($_SESSION['role'] === 'admin') {
            echo "<a href='read.php'>Gestionar Estudiantes</a>";
        } else if ($_SESSION['role'] === 'user') {
            echo "<a href='read.php'>Ver Estudiantes</a>";
        }

        echo "<a href='logout.php'>Cerrar Sesión</a>";
        ?>
    </div>
</body>
</html>