<?php
include 'includes/db_connection.php';
$data = json_decode(file_get_contents("php://input"), true);

$nombre = $data['nombre'];
$stock = $data['stock'];
$precio = $data['precio'];
$tamaño = $data['tamaño'];

$query = $conn->prepare("INSERT INTO productos (nombre, stock, precio, tamaño) VALUES (?, ?, ?, ?)");
$query->bind_param("siis", $nombre, $stock, $precio, $tamaño);

if ($query->execute()) {
    echo "Producto agregado con éxito.";
} else {
    echo "Error al agregar producto: " . $conn->error;
}
?>