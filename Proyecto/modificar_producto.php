<?php
include 'includes/db_connection.php';

$data = json_decode(file_get_contents("php://input"), true);

$id = $_GET['id'] ?? null;
$nombre = $data['nombre'] ?? null;
$stock = $data['stock'] ?? null;
$precio = $data['precio'] ?? null;
$tamaño = $data['tamaño'] ?? null;

// Validar datos
if (!$id || !$nombre || !$stock || !$precio || !$tamaño) {
    echo "Error: Datos incompletos.";
    exit;
}

$query = $conn->prepare("UPDATE productos SET nombre = ?, stock = ?, precio = ?, tamaño = ? WHERE id = ?");
$query->bind_param("sdssi", $nombre, $stock, $precio, $tamaño, $id);

if ($query->execute()) {
    echo "Producto modificado con éxito.";
} else {
    echo "Error al modificar el producto.";
}
?>