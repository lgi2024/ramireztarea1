<?php include 'includes/header.php'; ?>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<main>
    <section id="presentacion">
        <h2>Sobre Nosotros</h2>
        <p>Emprendimiento familiar dedicado al fraccionamiento de sal de consumo y elaboración de sal condimentada.</p>
        <p>Ofrecemos un producto natural y económico.</p>
    </section>
    <section id="producto-destacado">
        <h2>Producto Destacado</h2>
        <p>Nuestra sal condimentada: la mejor opción natural y económica.</p>
    </section>
</main>
<?php include 'includes/footer.php'; ?>