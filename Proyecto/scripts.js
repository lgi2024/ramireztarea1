// Mostrar mensaje de notificación
function mostrarMensaje(mensaje, tipo) {
    const divMensaje = document.getElementById('mensaje');
    divMensaje.textContent = mensaje;
    divMensaje.style.display = 'block';
    divMensaje.style.backgroundColor = tipo === 'exito' ? '#d4edda' : '#f8d7da';
    divMensaje.style.color = tipo === 'exito' ? '#155724' : '#721c24';

    // Ocultar mensaje después de 3 segundos
    setTimeout(() => {
        divMensaje.style.display = 'none';
    }, 3000);
}

// Función para mostrar el formulario de agregar producto
function mostrarFormularioAgregar() {
    console.log("Mostrar formulario activado");
    document.getElementById('formulario-agregar').style.display = 'block';
}

// Función para cerrar el formulario de agregar producto
function cerrarFormularioAgregar() {
    document.getElementById('formulario-agregar').style.display = 'none';
}

// Función para agregar un producto
function agregarProducto() {
    const nombre = document.getElementById('nombre').value;
    const stock = document.getElementById('stock').value;
    const precio = document.getElementById('precio').value;
    const tamaño = document.getElementById('tamaño').value;

    fetch('agregar_producto.php', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ nombre, stock, precio , tamaño })
    })
    .then(response => response.text())
    .then(data => {
        alert(data);
        location.reload();
    })
    .catch(error => console.error('Error:', error));
}

// Función para mostrar el modal con los datos del producto a modificar
function modificarProducto(id) {
    // Selecciona la fila correspondiente
    const row = document.querySelector(`[data-id='${id}']`);
    if (!row) {
        console.error("Fila no encontrada");
        return;
    }

    // Extrae los valores de las celdas
    const nombre = row.querySelector('.nombre').textContent;
    const stock = row.querySelector('.stock').textContent;
    const precio = row.querySelector('.precio').textContent;
    const tamaño = row.querySelector('.tamaño').textContent;

    // Rellena los campos del modal
    document.getElementById('id_producto').value = id;
    document.getElementById('edit_nombre').value = nombre;
    document.getElementById('edit_stock').value = stock;
    document.getElementById('edit_precio').value = precio;
    document.getElementById('edit_tamaño').value = tamaño;

    // Muestra el modal
    console.log("Modal debería mostrarse");
    document.getElementById('modificarProductoModal').style.display = 'flex';
}

// Función para cerrar el modal
function closeModal(modalId) {
    document.getElementById(modalId).style.display = 'none';
}

// Función para guardar los cambios del producto
function guardarCambios(event) {
    event.preventDefault(); // Evitar el envío del formulario por defecto

    const id = document.getElementById('id_producto').value;
    const nombre = document.getElementById('edit_nombre').value;
    const stock = document.getElementById('edit_stock').value;
    const precio = document.getElementById('edit_precio').value;
    const tamaño = document.getElementById('edit_tamaño').value;

    fetch(`modificar_producto.php?id=${id}`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ nombre, stock, precio, tamaño })
    })
    .then(response => response.text())
    .then(data => {
        mostrarMensaje(data, 'exito'); // Mostrar un mensaje
        closeModal('modificarProductoModal'); // Cerrar el modal
        location.reload(); // Recargar la página para actualizar la tabla
    })
    .catch(error => {
        console.error('Error:', error);
        mostrarMensaje('Error al modificar el producto.', 'error');
    });
}

// Función para eliminar un producto
function eliminarProducto(id) {
    if (confirm("¿Seguro que deseas eliminar este producto?")) {
        fetch(`eliminar_producto.php?id=${id}`, { method: 'POST' })
        .then(response => response.text())
        .then(data => {
            alert(data);
            location.reload();
        })
        .catch(error => console.error('Error:', error));
    }
}