<?php
include 'includes/db_connection.php';

// ID del producto desde los parámetros GET
$id = $_GET['id'];

// Consulta para eliminar el producto
$query = $conn->prepare("DELETE FROM productos WHERE id = ?");
$query->bind_param("i", $id);

if ($query->execute()) {
    echo "Producto eliminado con éxito.";
} else {
    echo "Error al eliminar el producto.";
}
?>