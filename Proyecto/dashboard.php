<?php 
session_start();
if (!isset($_SESSION['user_id'])) {
    header('Location: login.php');
    exit();
}

include 'includes/header.php'; 
?>

<div style="position: relative;">
    <a href="logout.php" style="position: absolute; top: 10px; right: 10px; font-size: 1em; color: red;">Cerrar Sesión</a>
</div>
<div style="text-align: center; margin-top: 50px;">
    <h1 style="font-size: 2em;">Panel de Administración</h1>
    <div style="margin-top: 30px;">
        <a href="inventario.php" style="display: block; margin: 20px; font-size: 1.5em;">Gestionar Inventario</a>
        <a href="ventas.php" style="display: block; margin: 20px; font-size: 1.5em;">Gestionar Ventas</a>
        <a href="clientes.php" style="display: block; margin: 20px; font-size: 1.5em;">Gestionar Clientes</a>
    </div>
</div>

<?php include 'includes/footer.php'; ?>