<?php include 'includes/header.php'; ?>
<main>
    <h2>Nuestros Productos</h2>
    <section id="catalogo">
        <div class="producto">
            <h3>Sal Fina</h3>
            <p>Precio: $1800 (fardo de 20 unidades)</p>
        </div>
        <div class="producto">
            <h3>Sal Entrefina</h3>
            <p>Precio: $1800 (fardo de 20 unidades)</p>
        </div>
        <div class="producto">
            <h3>Sal gruesa</h3>
            <p>Precio: $1800 (fardo de 20 unidades)</p>
        </div>
        <div class="producto">
            <h3>Sal Condimentada</h3>
            <p>Precio: $9000 (fardo de 20 unidades)</p>
        </div>
    </section>
</main>
<?php include 'includes/footer.php'; ?>