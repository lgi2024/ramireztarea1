<?php
include 'includes/header.php';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
include 'includes/db_connection.php';

// Verifica Administrador
if ($_SESSION['rol'] != 'admin') {
    echo "No tienes permiso para acceder a esta página.";
    exit;
}

// Consulta para obtener productos del inventario
$query = "SELECT id, nombre, stock, tipo, tamaño, precio FROM productos";
$result = $conn->query($query);

// Verifica si la consulta fue exitosa
if (!$result) {
    // Si la consulta falla, muestra un error
    echo "Error al consultar productos: " . $conn->error;
    exit;
}

// Verificar si se envía el formulario para modificar un producto existente
if (isset($_POST['modificar_producto'])) {
    $id_producto = $_POST['id_producto'];
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $precio = $_POST['precio'];
    $cantidad = $_POST['cantidad'];

    $sql = "UPDATE productos SET nombre = ?, descripcion = ?, precio = ?, cantidad = ? WHERE id_producto = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ssdii", $nombre, $descripcion, $precio, $cantidad, $id_producto);
    $stmt->execute();
    $stmt->close();

    header("Location: inventario.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventario de Productos</title>
    <div id="mensaje" style="display: none; padding: 10px; margin: 10px 0; border-radius: 5px;"></div>
    <link rel="stylesheet" href="css/style.css">
    <script src="scripts.js" defer></script>
    <style>
        /* Estilos para centrar el contenido */
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 10px;
            text-align: center;
        }

        h1 {
            margin-bottom: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 10px;
        }

        th, td {
            padding: 5px;
            border: 1px solid black;
            text-align: center;
        }

        .buttons-container {
            display: flex;
            justify-content: center;
            gap: 5px;
            margin-top: 10px;
        }

        #formulario-agregar {
            display: none;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Inventario de Productos</h1>

        <!-- Verifica si hay productos -->
        <?php if ($result->num_rows > 0): ?>
            <!-- Tabla de productos -->
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Stock</th>
                    <th>Precio</th>
                    <th>Tamaño</th>
                    <th>Acciones</th>
                </tr>
                <?php while ($row = $result->fetch_assoc()): ?>
                    <tr data-id="<?php echo $row['id']; ?>">
                    <td class="nombre"><?php echo $row['nombre']; ?></td>
                    <td class="stock"><?php echo $row['stock']; ?></td>
                    <td class="precio"><?php echo $row['precio']; ?></td>
                    <td class="tamaño"><?php echo $row['tamaño']; ?></td>
                        <td>
                            <button onclick="modificarProducto(<?php echo $row['id']; ?>)">Modificar</button>
                            <button onclick="eliminarProducto(<?php echo $row['id']; ?>)">Eliminar</button>
                        </td>
                    </tr>
                <?php endwhile; ?>
            </table>
        <?php else: ?>
            <!-- Si no hay productos en el inventario -->
            <p>No hay productos en el inventario.</p>
        <?php endif; ?>

        <!-- Botones adicionales debajo de la tabla -->
        <div class="buttons-container">
            <button onclick="mostrarFormularioAgregar()">Agregar Producto</button>
            <button onclick="window.location.href='dashboard.php'">Volver al Panel de Administración</button>
        </div>

        <!-- Formulario para agregar producto -->
        <div id="formulario-agregar">
            <h2>Agregar Producto</h2>
            <form id="form-agregar">
                <label>Nombre: <input type="text" id="nombre" required></label><br>
                <label>Stock: <input type="number" id="stock" required></label><br>
                <label>Precio: <input type="number" step="0.01" id="precio" required></label><br>
                <label>Tamaño: <input type="text" id="tamaño" required></label><br>
                <button type="button" onclick="agregarProducto()">Agregar</button>
                <button type="button" onclick="cerrarFormularioAgregar()">Cancelar</button>
            </form>
        </div>
    </div>

    <!-- Modal para modificar producto -->
    <div id="modificarProductoModal" class="modal" style="display: none;">
        <div class="modal-content">
            <span class="close" onclick="closeModal('modificarProductoModal')">&times;</span>
            <h2>Modificar Producto</h2>
            <form id="form-modificar" onsubmit="guardarCambios(event)">
                <input type="hidden" id="id_producto" name="id_producto">
                
                <label>Nombre del Producto:</label><br>
                <input type="text" id="edit_nombre" name="nombre" required><br><br>
                
                <label>Stock:</label><br>
                <input type="number" id="edit_stock" name="stock" required><br><br>
                
                <label>Precio:</label><br>
                <input type="number" id="edit_precio" name="precio" step="0.01" required><br><br>
                
                <label>Tamaño:</label><br>
                <input type="text" id="edit_tamaño" name="tamaño" required><br><br>
                
                <button type="submit">Guardar Cambios</button>
            </form>
        </div>
    </div>

    <style>
        /* Estilos del modal */
        .modal {
        display: none; /* Oculto por defecto */
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 1000;
        justify-content: center;
        align-items: center;
        }

        .modal-content {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            width: 400px;
            text-align: center;
        }
    </style>
</body>
</html>