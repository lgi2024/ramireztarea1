<?php
include 'includes/header.php';
session_start();
include 'includes/db_connection.php';

// Verifica Administrador
if ($_SESSION['rol'] != 'admin') {
    echo "No tienes permiso para acceder a esta página.";
    exit;
}

// Consulta para obtener productos del inventario
$query = "SELECT * FROM ventas";
$result = $conn->query($query);

// Verifica si la consulta fue exitosa
if (!$result) {
    // Si la consulta falla, muestra un error
    echo "Error al consultar productos: " . $conn->error;
    exit;
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventario de Productos</title>
    <link rel="stylesheet" href="css/style.css">
    <style>
        /* Estilos para centrar el contenido */
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 10px;
            text-align: center;
        }

        h1 {
            margin-bottom: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 10px;
        }

        th, td {
            padding: 5px;
            border: 1px solid black;
            text-align: center;
        }

        .buttons-container {
            display: flex;
            justify-content: center;
            gap: 5px;
            margin-top: 10px;
        }

        #formulario-agregar {
            display: none;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Lista de Ventas</h1>

        <!-- Verifica si hay productos -->
        <?php if ($result->num_rows > 0): ?>
            <!-- Tabla de productos -->
            <table>
                <tr>
                    <th>ID Cliente</th>
                    <th>Fecha</th>
                    <th>Total</th>
                </tr>
                <?php while ($row = $result->fetch_assoc()): ?>
                    <tr>
                        <td><?php echo $row['cliente_id']; ?></td>
                        <td><?php echo $row['fecha']; ?></td>
                        <td><?php echo $row['total']; ?></td>
                        <td>
                            <button onclick="modificarProducto(<?php echo $row['id']; ?>)">Modificar</button>
                            <button onclick="eliminarProducto(<?php echo $row['id']; ?>)">Eliminar</button>
                        </td>
                    </tr>
                <?php endwhile; ?>
            </table>
        <?php else: ?>
            <!-- Si no hay productos en el inventario -->
            <p>No hay ventas.</p>
        <?php endif; ?>

        <!-- Botones adicionales debajo de la tabla -->
        <div class="buttons-container">
            <button onclick="mostrarFormularioAgregar()">Agregar Venta</button>
            <button onclick="window.location.href='dashboard.php'">Volver al Panel de Administración</button>
        </div>

        <!-- Formulario para agregar producto -->
        <div id="formulario-agregar">
            <h2>Agregar Producto</h2>
            <form id="form-agregar">
                <label>Nombre: <input type="text" id="nombre" required></label><br>
                <label>Stock: <input type="number" id="stock" required></label><br>
                <label>Precio: <input type="number" step="0.01" id="precio" required></label><br>
                <label>Tamaño: <input type="text" id="tamaño" required></label><br>
                <button type="button" onclick="agregarProducto()">Agregar</button>
                <button type="button" onclick="cerrarFormularioAgregar()">Cancelar</button>
            </form>
        </div>
    </div>
    
</body>
</html> 